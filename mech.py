from random import randint, choice


class Mechanic(object):
    field = []
    is_finished = False
    message = ""

    def __init__(self, f):
        self.height = f
        self.width = f
        self.create_field()
        self.start()

    def start(self):
        self.point()
        self.point()

    def create_field(self):
        for x in range(self.width):
            self.field.append([0] * self.height)

    def point(self):
        zero_points = list(self.list_of_zero())
        if not zero_points:
            self.is_finished = True
            self.message = "You lose!"
        x, y = choice(zero_points)
        self.field[x][y] = 4 if randint(0, 9) > 8 else 2

    def print_field(self):
        for row in self.field:
            print "".join(str(row))
        print " "

    def list_of_zero(self):
        for i in range(len(self.field)):
            for j in range(len(self.field[i])):
                if self.field[i][j] == 0:
                    yield i, j

    def move(self, direction):
        moved = False
        self.rotate(direction+1)
        l = []
        for line in self.field:
            while len(line) and line[-1] == 0:
                line.pop(-1)
            i = len(line)-1
            while i >= 0:
                if line[i] == 0:
                    moved = True
                    line.pop(i)
                i -= 1
            accum = 0
            result = []
            for el in line:
                if accum:
                    if accum == el:
                        result.append(accum+el)
                        moved = True
                        accum = 0
                    else:
                        result.append(accum)
                        accum = el
                else:
                    accum = el
            if accum:
                result.append(accum)
            while len(result) < len(self.field):
                result.append(0)
            l.append(result)
        self.field = l
        self.rotate(0 - (direction+1))
        if moved:
            self.point()
            self.win()

    def win(self):
        for i in range(len(self.field)):
            if max(self.field[i]) == 16:
                self.is_finished = True
                self.message = "You win!"

    def rotate(self, num):
        num %= 4
        if num == 0:
            self.field = self.field
        elif num == 1:
            self.field = map(list, zip(*self.field[::-1]))
        elif num == 2:
            self.field.reverse()
            for i in self.field:
                i.reverse()
        elif num == 3:
            self.field = map(list, zip(*self.field)[::-1])


def key(s):
    if s == "w":
        return 2
    elif s == "s":
        return 0
    elif s == "d":
        return 1
    elif s == "a":
        return 3
    else:
        return False


if __name__ == "__main__":
    my_game = Mechanic(4)
    my_game.print_field()
    while not my_game.is_finished:
            d = key(raw_input(""))
            if type(d) == int:
                my_game.move(d)
                my_game.print_field()
            else:
                print "Wrong button"
    else:
        print my_game.message